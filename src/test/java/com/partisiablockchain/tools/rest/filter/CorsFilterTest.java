package com.partisiablockchain.tools.rest.filter;

/*-
 * #%L
 * rest
 * %%
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerResponseContext;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.Response;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

/** Test. */
public class CorsFilterTest {

  private CorsFilter corsFilter;
  private ContainerRequestContext requestMock;
  private ContainerResponseContext responseMock;
  private MultivaluedHashMap<String, Object> headerMap;

  /** Construct mocks for request and response. */
  @BeforeEach
  public void setUp() {
    corsFilter = new CorsFilter();

    requestMock = Mockito.mock(ContainerRequestContext.class);
    responseMock = Mockito.mock(ContainerResponseContext.class);

    headerMap = new MultivaluedHashMap<>();
    Mockito.when(responseMock.getHeaders()).thenReturn(headerMap);
  }

  @Test
  public void filterOptions() {
    Mockito.when(requestMock.getMethod()).thenReturn("OPTIONS");

    corsFilter.filter(requestMock);

    Mockito.verify(requestMock)
        .abortWith(
            Mockito.argThat(
                response -> response.getStatus() == Response.Status.OK.getStatusCode()));

    Assertions.assertThat(headerMap.isEmpty()).isEqualTo(true);

    corsFilter.filter(requestMock, responseMock);

    Assertions.assertThat(headerMap.size()).isEqualTo(4);
  }

  @Test
  public void filterOther() {
    Mockito.when(requestMock.getMethod()).thenReturn("GET");

    corsFilter.filter(requestMock);

    Mockito.verify(requestMock, Mockito.never()).abortWith(Mockito.any());

    Assertions.assertThat(headerMap.isEmpty()).isEqualTo(true);

    corsFilter.filter(requestMock, responseMock);

    Assertions.assertThat(headerMap.size()).isEqualTo(4);
  }
}
