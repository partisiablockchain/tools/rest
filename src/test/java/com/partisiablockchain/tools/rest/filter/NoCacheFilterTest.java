package com.partisiablockchain.tools.rest.filter;

/*-
 * #%L
 * rest
 * %%
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerResponseContext;
import jakarta.ws.rs.container.ResourceInfo;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.MultivaluedHashMap;
import java.lang.reflect.Method;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

/** Test. */
public class NoCacheFilterTest {

  private NoCacheFilter noCacheFilter;
  private ContainerRequestContext requestMock;
  private ContainerResponseContext responseMock;
  private MultivaluedHashMap<String, Object> headerMap;

  /** Setup a no cache filter and mocks for request and response. */
  @BeforeEach
  public void setUp() {
    noCacheFilter = new NoCacheFilter(new TestResourceInfo("withoutCacheControl"));

    requestMock = Mockito.mock(ContainerRequestContext.class);
    responseMock = Mockito.mock(ContainerResponseContext.class);

    headerMap = new MultivaluedHashMap<>();
    Mockito.when(responseMock.getHeaders()).thenReturn(headerMap);
  }

  @Test
  public void filter() {
    noCacheFilter.filter(requestMock, responseMock);

    Assertions.assertThat(headerMap.getFirst(HttpHeaders.EXPIRES)).isNotNull();
    Assertions.assertThat(headerMap.getFirst(HttpHeaders.CACHE_CONTROL)).isNotNull();
  }

  @Test
  public void cacheControlPresent() {
    noCacheFilter = new NoCacheFilter(new TestResourceInfo("publicImmutable"));
    noCacheFilter.filter(requestMock, responseMock);

    Assertions.assertThat(headerMap.getFirst(HttpHeaders.EXPIRES)).isNull();
    Assertions.assertThat(headerMap.getFirst(HttpHeaders.CACHE_CONTROL))
        .isEqualTo("public, max-age=604800, immutable");
  }

  @Test
  public void buildCacheControlHeader() {
    assertHeader("publicImmutable", "public, max-age=604800, immutable");
    assertHeader("noStore", "no-store");
    assertHeader("expiresSoon", "public, max-age=15");
  }

  @Test
  public void methodInfoNull() {
    noCacheFilter = new NoCacheFilter(new TestResourceInfo("nonExistingMethod"));
    noCacheFilter.filter(requestMock, responseMock);

    Assertions.assertThat(headerMap.getFirst(HttpHeaders.EXPIRES)).isEqualTo(0);
    Assertions.assertThat(headerMap.getFirst(HttpHeaders.CACHE_CONTROL))
        .isEqualTo("no-store, must-revalidate");
  }

  private static void assertHeader(String methodName, String expectedHeader) {
    Method method = getMethod(methodName);
    CacheControl annotation = method.getAnnotation(CacheControl.class);
    String actual = NoCacheFilter.buildCacheControlHeader(annotation);
    Assertions.assertThat(actual).isEqualTo(expectedHeader);
  }

  private static Method getMethod(String methodName) {
    try {
      return Annotated.class.getMethod(methodName);
    } catch (NoSuchMethodException e) {
      return null;
    }
  }

  @SuppressWarnings("unused")
  private static final class Annotated {

    public void withoutCacheControl() {
      // Intentionally left empty
    }

    @CacheControl(directive = CacheControl.Directive.PUBLIC, maxAge = 604800, immutable = true)
    public void publicImmutable() {
      // Intentionally left empty
    }

    @CacheControl(directive = CacheControl.Directive.NO_STORE)
    public void noStore() {
      // Intentionally left empty
    }

    @CacheControl(directive = CacheControl.Directive.PUBLIC, maxAge = 15)
    public void expiresSoon() {
      // Intentionally left empty
    }
  }

  private static final class TestResourceInfo implements ResourceInfo {

    private final Method method;

    public TestResourceInfo(String methodName) {
      this.method = getMethod(methodName);
    }

    @Override
    public Method getResourceMethod() {
      return method;
    }

    @Override
    public Class<?> getResourceClass() {
      return Annotated.class;
    }
  }
}
