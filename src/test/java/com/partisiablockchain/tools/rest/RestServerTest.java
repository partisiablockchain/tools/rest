package com.partisiablockchain.tools.rest;

/*-
 * #%L
 * rest
 * %%
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.function.Function;
import org.glassfish.jersey.server.ResourceConfig;
import org.junit.jupiter.api.Test;

/** Test. */
public final class RestServerTest extends AbstractServerTest {

  @Test
  public void start() throws InterruptedException {
    testStart(RestServer::new);
  }

  @Test
  public void startStopCombinations() {
    RestServer server = new RestServer(port(), new ResourceConfig());
    testStartStopCombinations(server);
  }

  @Test
  public void emptyConfig() throws NoSuchFieldException, IllegalAccessException {
    Function<ResourceConfig, RestServer> createServer =
        (ResourceConfig config) -> new RestServer(port(), config);
    testEmptyConfig(createServer);
  }

  @Test
  public void populatedConfig() throws NoSuchFieldException, IllegalAccessException {
    Function<ResourceConfig, RestServer> createServer =
        (ResourceConfig config) -> new RestServer(port(), config);
    testPopulatedConfig(createServer);
  }
}
