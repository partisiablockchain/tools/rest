package com.partisiablockchain.tools.rest;

/*-
 * #%L
 * rest
 * %%
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ObjectMapperProviderTest {

  @Test
  public void objectMapperVisibility() throws JsonProcessingException {
    ObjectMapper objectMapper = createDefaultMapper();
    TestRecord testRecord = new TestRecord("foo", 123);
    TestClass testClass = new TestClass("secret", "foo", 123);
    String jsonRecord = objectMapper.writeValueAsString(testRecord);
    String jsonClass = objectMapper.writeValueAsString(testClass);
    TestRecord readRecord = objectMapper.readValue(jsonRecord, TestRecord.class);
    TestClass readClass = objectMapper.readValue(jsonClass, TestClass.class);

    String expectedJson = "{\"foo\":\"foo\",\"bar\":123}";
    Assertions.assertThat(jsonRecord).isEqualTo(expectedJson);
    Assertions.assertThat(jsonClass).isEqualTo(expectedJson);
    Assertions.assertThat(readRecord).isEqualTo(testRecord);
    Assertions.assertThat(readClass)
        .usingRecursiveComparison()
        .ignoringFields("hidden")
        .isEqualTo(testClass);
  }

  private static ObjectMapper createDefaultMapper() {
    ObjectMapperProvider provider = new ObjectMapperProvider();
    return provider.getContext(ObjectMapperProviderTest.class);
  }

  @SuppressWarnings("unused")
  record TestRecord(String foo, int bar) {}

  @SuppressWarnings("unused")
  static final class TestClass {
    private final String hidden;
    private final String foo;
    public final int bar;

    private TestClass() {
      this.hidden = null;
      this.foo = null;
      this.bar = 0;
    }

    TestClass(String hidden, String foo, int bar) {
      this.hidden = hidden;
      this.foo = foo;
      this.bar = bar;
    }

    public String getFoo() {
      return foo;
    }
  }
}
