package com.partisiablockchain.tools.rest;

/*-
 * #%L
 * rest
 * %%
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import jakarta.ws.rs.core.Response.Status;
import java.util.Optional;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class RootResourceTest {

  @Test
  public void defaultConstructor() {
    RootResource root = new RootResource(Optional.empty());
    Assertions.assertThat(root.status().getStatusInfo()).isEqualTo(Status.NO_CONTENT);
  }

  @Test
  public void error() {
    RootResource root = new RootResource(Optional.of(() -> false));
    Assertions.assertThat(root.status().getStatusInfo()).isEqualTo(Status.INTERNAL_SERVER_ERROR);
  }
}
