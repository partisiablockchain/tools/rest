package com.partisiablockchain.tools.rest;

/*-
 * #%L
 * rest
 * %%
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import jakarta.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.function.Function;
import org.assertj.core.api.Assertions;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.server.ResourceConfig;
import org.junit.jupiter.api.Test;

/** Test. */
public final class GrizzlyRestServerTest extends AbstractServerTest {

  @Test
  public void start() throws InterruptedException {
    testStart(GrizzlyRestServer::new);
  }

  @Test
  public void startStopCombinations() {
    GrizzlyRestServer server = new GrizzlyRestServer(port(), new ResourceConfig());
    testStartStopCombinations(server);
  }

  @Test
  public void emptyConfig() throws NoSuchFieldException, IllegalAccessException {
    Function<ResourceConfig, GrizzlyRestServer> createServer =
        (ResourceConfig config) -> new GrizzlyRestServer(port(), config);
    testEmptyConfig(createServer);
  }

  @Test
  public void populatedConfig() throws NoSuchFieldException, IllegalAccessException {
    Function<ResourceConfig, GrizzlyRestServer> createServer =
        (ResourceConfig config) -> new GrizzlyRestServer(port(), config);
    testPopulatedConfig(createServer);
  }

  @Test
  void closeServerShutdownNow() {
    ResourceConfig config = new ResourceConfig();
    GrizzlyRestServer grizzlyRestServer = new GrizzlyRestServer(config);
    URI baseUri = UriBuilder.fromUri("http://0.0.0.0/").port(8080).build();
    HttpServer httpServer = grizzlyRestServer.createServer(baseUri, config);

    grizzlyRestServer.closeServer(httpServer, 0);

    Assertions.assertThat(httpServer.isStarted()).isFalse();
  }
}
