package com.partisiablockchain.tools.rest;

/*-
 * #%L
 * rest
 * %%
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import jakarta.ws.rs.core.UriBuilder;
import java.net.URI;
import org.glassfish.jersey.server.ResourceConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Abstract REST server. */
abstract class AbstractRestServer<T> implements AutoCloseable {

  private static final Logger logger = LoggerFactory.getLogger(AbstractRestServer.class);
  private final int port;
  private final ResourceConfig config;
  private T httpServer;
  private int delay = 1;

  /**
   * Constructor with default port and original config. A new config will be created based on the
   * original and default resources will be added to the new config including RootResource.
   *
   * @param config defines the functionality of the server
   */
  protected AbstractRestServer(ResourceConfig config) {
    this(8080, config);
  }

  /**
   * Constructor with port and original config. A new config will be created based on the original
   * and default resources will be added to the new config
   *
   * @param port of server
   * @param config defines the functionality of the server
   */
  protected AbstractRestServer(int port, ResourceConfig config) {
    this.port = port;
    this.config = new ResourceConfig(config);
    for (Class<?> resource : RestResources.DEFAULT) {
      if (!this.config.isRegistered(resource)) {
        this.config.register(resource);
      }
    }
  }

  /**
   * Sets delay for closing server.
   *
   * @param delay maximum time in seconds to wait for the server closing
   */
  public void setDelay(int delay) {
    this.delay = delay;
  }

  /**
   * Checks server status.
   *
   * @return true if the server has been started, false otherwise
   */
  public boolean isRunning() {
    return httpServer != null;
  }

  /** Starts the server. Ensuring that the default resources has been registered */
  public void start() {
    if (!isRunning()) {
      logger.info("Starting server");
      URI baseUri = UriBuilder.fromUri("http://0.0.0.0/").port(port).build();
      httpServer = createServer(baseUri, config);
    }
  }

  /** Closes the server. */
  @Override
  public void close() {
    if (isRunning()) {
      logger.info("Closing server within " + delay + " seconds");
      closeServer(httpServer, delay);
      httpServer = null;
    }
  }

  /**
   * Creates the server.
   *
   * @param baseUri the uri of the server
   * @param config defines the functionality of the server
   * @return the server
   */
  abstract T createServer(URI baseUri, ResourceConfig config);

  /**
   * Closes the server.
   *
   * @param httpServer the server to close
   * @param delay the delay before closing the server
   */
  abstract void closeServer(T httpServer, int delay);
}
