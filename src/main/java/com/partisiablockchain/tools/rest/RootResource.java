package com.partisiablockchain.tools.rest;

/*-
 * #%L
 * rest
 * %%
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import java.util.Optional;
import org.glassfish.jersey.server.ResourceConfig;

/** Generic is alive service for any web application. */
@Path("/")
public final class RootResource {

  private final AliveCheck aliveCheck;

  /**
   * Default constructor. To set aliveCheck register an {@link AliveCheckBinder} to the {@link
   * ResourceConfig}.
   *
   * @param aliveCheck found in {@link ResourceConfig} if registered
   */
  @Inject
  public RootResource(Optional<AliveCheck> aliveCheck) {
    this.aliveCheck = aliveCheck.orElseGet(() -> () -> true);
  }

  /**
   * Checks the status of this resource, if available returns 204. An application can use arbitrary
   * complicated patterns to check if the server is alive - supplied at construction.
   *
   * @return {@link Status#NO_CONTENT} if available.
   */
  @GET
  public Response status() {
    if (aliveCheck.check()) {
      return Response.noContent().build();
    } else {
      return Response.status(Status.INTERNAL_SERVER_ERROR).build();
    }
  }
}
