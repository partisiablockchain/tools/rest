package com.partisiablockchain.tools.rest.filter;

/*-
 * #%L
 * rest
 * %%
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

/** Used to defined constants used as priority for registered filters. */
public interface FilterPriorities {

  /** Priority for logging. */
  int LOGGING_FILTER_PRIORITY = 50;

  /** Priority for no cache. */
  int NO_CACHE_PRIORITY = 75;

  /** Priority for cors. */
  int CORS_FILTER_PRIORITY = 100;
}
