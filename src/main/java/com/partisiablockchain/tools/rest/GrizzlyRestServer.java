package com.partisiablockchain.tools.rest;

/*-
 * #%L
 * rest
 * %%
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.net.URI;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.glassfish.grizzly.GrizzlyFuture;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

/** Rest server configured to use Grizzly. */
public final class GrizzlyRestServer extends AbstractRestServer<HttpServer> {

  /**
   * Grizzly rest server with default port.
   *
   * @param config defines the functionality of the server
   */
  public GrizzlyRestServer(ResourceConfig config) {
    super(config);
  }

  /**
   * Grizzly rest server with custom port and config.
   *
   * @param port of the server
   * @param config defines the functionality of the server
   */
  public GrizzlyRestServer(int port, ResourceConfig config) {
    super(port, config);
  }

  @Override
  HttpServer createServer(URI baseUri, ResourceConfig config) {
    return GrizzlyHttpServerFactory.createHttpServer(baseUri, config);
  }

  @Override
  void closeServer(HttpServer httpServer, int delay) {
    GrizzlyFuture<HttpServer> shutdown = httpServer.shutdown();
    try {
      shutdown.get(delay, TimeUnit.SECONDS);
    } catch (TimeoutException | InterruptedException | ExecutionException e) {
      httpServer.shutdownNow();
    }
  }
}
