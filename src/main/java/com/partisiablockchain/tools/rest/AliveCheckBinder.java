package com.partisiablockchain.tools.rest;

/*-
 * #%L
 * rest
 * %%
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.glassfish.jersey.internal.inject.AbstractBinder;

/**
 * To use a version of {@link AliveCheck}, register an instance of this class to the {@link
 * org.glassfish.jersey.server.ResourceConfig}.
 */
public final class AliveCheckBinder extends AbstractBinder {

  private final AliveCheck aliveCheck;

  /**
   * Creates a new Alive checker binder.
   *
   * @param aliveCheck the alive check giving the status
   */
  public AliveCheckBinder(AliveCheck aliveCheck) {
    this.aliveCheck = aliveCheck;
  }

  @Override
  protected void configure() {
    bind(aliveCheck).to(AliveCheck.class);
  }
}
